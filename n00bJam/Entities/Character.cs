﻿using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace n00bJam
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Character: Entity
    {
        float gravity = 1f;
        float jumpPower = 1f;

        public Character(Vector2 position, bool saveable, SpriteSheet defaultSprite = null): 
            base(position, saveable, defaultSprite)
        {
            LoadAnimation("Spritesheets/idle@9", "default", true);
            LoadAnimation("Spritesheets/walk@5", "walk", true);
            LoadAnimation("Spritesheets/jump@7", "jump", false);
            Show("default");
        }

        public void WalkRight() {
            Sprites["walk"].Mirror = false;
            Show("walk");
            this.velocity = new Vector2(1, 0);
        }

        public void WalkLeft()
        {
            Sprites["walk"].Mirror = true;
            Show("walk");
            this.velocity = new Vector2(-1, 0);
        }

        public void FallDown() {
            //Show("jump");
            this.velocity += new Vector2(0, 1);
            this.position += new Vector2(0, gravity);
        }
        
        public void Jump() {
            Show("jump");
            this.position -= new Vector2(0, jumpPower);
        }

        public override void Update(GameTime gameTime)
        {
            this.position += new Vector2(0, 1);
            if (!CheckIntersect())
                FallDown();
            this.position -= new Vector2(0, 1);
            base.Update(gameTime);
        }
    }
}
