﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace n00bJam
{
    public class DefaultController: Controller
    {
        public override void HandleInput(GameTime gameTime)
        {
            if (InputHandler.Is(Keys.D, Button.Down)) {
                GameState.Active.Player.WalkRight();
            }
            if (InputHandler.Is(Keys.A, Button.Down)) {
                GameState.Active.Player.WalkLeft();
            }
            if (InputHandler.Is(Keys.Space, Button.Pressed)) {
                GameState.Active.Player.Jump();
            }
        }

        public override void Update(GameTime gameTime)
        {
        }
    }
}
