﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace n00bJam
{
    public interface AttrPhysics
    {
        Rectangle Bounds { get; }
        bool[,] Pixels { get; }

        bool CheckIntersect(IEnumerable<AttrPhysics> query);
    }

    public abstract class AttrPhysicsClass: Attribute<AttrPhysics>, AttrPhysics
    {
        public AttrPhysicsClass(AttrPhysics parent):
            base(parent)
        {
        }

        public Rectangle Bounds
        {
            get {
                return parent.Bounds;
            }
        }

        public abstract bool[,] Pixels { get; }

        public abstract bool CheckIntersect(IEnumerable<AttrPhysics> query);
    }

    public class NoPhysics: AttrPhysicsClass
    {
        public NoPhysics(AttrPhysics parent) : base(parent)
        {
        }

        public override bool[,] Pixels
        {
            get {
                return new bool[0, 0];
            }
        }

        public override bool CheckIntersect(IEnumerable<AttrPhysics> query)
        {
            return false;
        }
    }

    public class BoxPhysics: AttrPhysicsClass
    {
        public BoxPhysics(AttrPhysics parent):
            base(parent)
        {
        }

        private bool[,] pixelBuffer = null;

        private bool[,] GeneratePixels()
        {
            bool[,] pixels = new bool[Bounds.Width, Bounds.Height];
            for (int x = 0; x < Bounds.Width; ++x)
                for (int y = 0; y < Bounds.Height; ++y)
                    pixels[x, y] = true;
            return pixels;
        }

        public override bool[,] Pixels
        {
            get {
                if (pixelBuffer != null)
                    return pixelBuffer;
                else
                    return GeneratePixels();
            }
        }

        public override bool CheckIntersect(IEnumerable<AttrPhysics> query)
        {
            foreach (BoxPhysics elem in query) {
                if (this.Bounds.Intersects(elem.Bounds))
                    return true;
            }
            pixelBuffer = GeneratePixels();
            foreach (PixelPhysics elem in query) {
                if (elem.CheckIntersect(new AttrPhysics[1] { this })) {
                    pixelBuffer = null;
                    return true;
                }
            }
            pixelBuffer = null;
            return false;
        }
    }


    public class PixelPhysics: AttrPhysicsClass
    {
        public PixelPhysics(AttrPhysics parent):
            base(parent)
        {
        }

        public override bool[,] Pixels
        {
            get {
                return parent.Pixels;
            }
        }

        public override bool CheckIntersect(IEnumerable<AttrPhysics> query)
        {
            foreach (AttrPhysics elem in query.Where(attr => !(attr is NoPhysics))) {
                if (!this.Bounds.Intersects(elem.Bounds))
                    continue;
                Rectangle isect = Rectangle.Intersect(this.Bounds, elem.Bounds);

                for (int x = 0; x < isect.Width; ++x) {
                    for (int y = 0; x < isect.Height; ++y) {
                        if (!this.Pixels[x - this.Bounds.Left, y - this.Bounds.Top]) continue;
                        if (!elem.Pixels[x - elem.Bounds.Left, y - elem.Bounds.Top]) continue;
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
