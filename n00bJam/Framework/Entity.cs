﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

namespace n00bJam
{
    /// <summary>
    /// A visible object in-game
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Entity: AttrPhysics
    {
        public AttrPhysics attrPhysics;

        public bool CheckIntersect(IEnumerable<AttrPhysics> query = null)
        {
            return attrPhysics.CheckIntersect(query ?? GameState.Active.Entities.Where(elem => elem != this));
        }

        [JsonProperty("position")]
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        [JsonProperty("velocity")]
        public Vector2 Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public bool[,] Pixels
        {
            get {
                bool[,] temp = new bool[this.Current.Width, this.Current.Height];
                for (int x = 0; x < this.Current.Width; ++x)
                    for (int y = 0; y < this.Current.Width; ++y)
                        temp[x, y] = Current.IsTranslucent(x, y);
                return temp;
            }
        }
        public Rectangle Bounds
        {
            get {
                if (this.Current != null)
                    return new Rectangle(position.ToPoint(), new Point(this.Current.Width, this.Current.Height));
                else
                    return Rectangle.Empty;
            }
        }

        public List<Level> LinkedLevels = new List<Level>();

        [JsonProperty("id")]
        public string Id = "";

        protected Vector2 velocity;
        protected Vector2 position;
        protected Dictionary<string, SpriteSheet> Sprites { get; private set; }
        protected SpriteSheet Current { get; private set; }

        public Entity(Vector2 Position, bool Saveable, SpriteSheet defaultSprite = null)
        {
            this.attrPhysics = new BoxPhysics(this);

            this.velocity = Vector2.Zero;
            this.position = Position;
            this.Sprites = new Dictionary<string, SpriteSheet>();
            this.Current = defaultSprite;
            Sprites["default"] = defaultSprite;
        }

        public void LoadAnimation(string assetName, string id, bool looping, float frameTime = 0.1f)
        {
            Animation anim = new Animation(assetName, looping, frameTime);
            Sprites[id] = anim;
        }

        public void LoadSprite(string assetName, string id, int sheetIndex = 0)
        {
            SpriteSheet sprite = new SpriteSheet(assetName, sheetIndex);
            Sprites[id] = sprite;
        }

        public void Show(string id)
        {
            if (Current != Sprites[id]) {
                Current = Sprites[id];
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            position += velocity * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            this.Current?.Draw(spriteBatch, gameTime, this.position);
        }
    }
}
