﻿namespace n00bJam
{
    public abstract class Attribute<T>
    {
        public Attribute(T parent)
        {
            this.parent = parent;
        }
        protected T parent { get; }
    }
}
