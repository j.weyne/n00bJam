﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace n00bJam
{
    public class State: IEnumerable<Entity>, IDisposable
    {
        public List<Entity> Entities = new List<Entity>();
        public List<Controller> Controllers = new List<Controller>();

        protected static List<State> states = new List<State>();

        public State()
        {
            states.Add(this);
        }
        
        public static State Active
        {
            get {
                return states[states.Count - 1];
            }
        }

        public static void Update(GameTime gameTime)
        {
            foreach (State s in states) {
                foreach (Controller c in s.Controllers)
                    c.HandleInput(gameTime);

                foreach (Controller c in s.Controllers)
                    c.Update(gameTime);

                foreach (Entity e in s.Entities)
                    e.Update(gameTime);
            }
        }

        public static void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            foreach (State s in states)
                foreach (Entity e in s.Entities)
                    e.Draw(gameTime, spriteBatch);
            spriteBatch.End();
        }

        public IEnumerator<Entity> GetEnumerator()
        {
            return Entities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Entities.GetEnumerator();
        }

        #region IDisposable Support
        private bool disposed = false; // To detect redundant calls
        
        public void Dispose()
        {
            if (!disposed) {
                states.Remove(this);
                disposed = true;
            }
        }
        #endregion
    }
}
