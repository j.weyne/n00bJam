﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace n00bJam
{
    public class Animation: SpriteSheet
    {
        protected float frameTime;
        protected bool isLooping;
        protected float time;

        public Animation(string assetname, bool isLooping, float frameTime = 0.1f):
            base(assetname)
        {
            this.frameTime = frameTime;
            this.isLooping = isLooping;
        }

        public override bool NextFrame {
            get { return !isLooping && sheetIndex >= FrameCount - 1; }
        }

        public override void Reset()
        {
            sheetIndex = 0;
            time = 0.0f;
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Vector2 position)
        {
            time += (float)gameTime.ElapsedGameTime.TotalSeconds;
            while (time > frameTime)
            {
                time -= frameTime;
                if (isLooping)
                {
                    sheetIndex = (sheetIndex + 1) % FrameCount;
                }
                else
                {
                    sheetIndex = Math.Min(sheetIndex + 1, FrameCount - 1);
                }
            }
            base.Draw(spriteBatch, gameTime, position);
        }

        public float FrameTime
        {
            get { return frameTime; }
        }

        public bool IsLooping
        {
            get { return isLooping; }
        }
    }
}
