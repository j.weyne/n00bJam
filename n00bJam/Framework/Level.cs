﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using Newtonsoft.Json.Linq;
using Microsoft.Xna.Framework;

namespace n00bJam
{
    /// <summary>
    /// Represents a level
    /// </summary>
    public class Level
    {
        public static List<Level> All { get; } = new List<Level>();
        private static bool buffered = false;

        public static void Buffer()
        {
            if (!buffered) {
                IEnumerable<string> lvlFiles = Directory.EnumerateFiles("Content/Levels").Where(file => file.EndsWith(".json"));
                Dictionary<Level, IEnumerable<JToken>> neighbourTrees = new Dictionary<Level, IEnumerable<JToken>>();
                // Create levels
                foreach (string currentFile in lvlFiles ?? Enumerable.Empty<string>()) {
                    JObject tree = JObject.Parse(File.ReadAllText(currentFile));
                    Level lvl = new Level(tree);
                    neighbourTrees.Add(lvl, tree["neighbours"]?.Children() ?? Enumerable.Empty<JToken>());
                }
                // Link levels by neighbours
                foreach (KeyValuePair<Level, IEnumerable<JToken>> kvp in neighbourTrees) {
                    foreach (JProperty kvpNeighbours in kvp.Value)
                        kvp.Key.Neighbours.Add(All.Single(lvl => lvl.Id == kvpNeighbours.Name), kvpNeighbours.Value.ToObject<Rectangle>());
                }
                buffered = true;
            }
        }

        public static bool LoadLevels(Point point)
        {
            if (GameState.Active.Current == null) {
                Level level = All.First(lvl => lvl.Bounds.Contains(point));
                level.Load();
                GameState.Active.Current = level;
            }
            // Load neighbouring levels
            foreach (KeyValuePair<Level, Rectangle> kvp in GameState.Active.Current.Neighbours) {
                if (kvp.Value.Contains(point))
                    kvp.Key.Load();
            }
            // If not in this level anymore, switch level
            if (!GameState.Active.Current.Bounds.Contains(point)) {
                foreach (KeyValuePair<Level, Rectangle> kvp in GameState.Active.Current.Neighbours) {
                    if (kvp.Key.Bounds.Contains(point)) {
                        GameState.Active.Current = kvp.Key;
                        foreach (KeyValuePair<Level, Rectangle> kvpNested in GameState.Active.Current.Neighbours) {
                            kvp.Key.Loaded = kvpNested.Value.Contains(point);
                        }
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        public Level(JObject tree)
        {
            All.Add(this);
            this.Id = tree["id"].ToString();
            int[] size = tree["size"].ToString().Split(',').Select(x => int.Parse(x.Trim())).ToArray();
            this.Size = new Point(size[0], size[1]);
            int[] location = tree["location"].ToString().Split(',').Select(x => int.Parse(x.Trim())).ToArray();
            this.Location = new Point(location[0], location[1]);
            this.EntityTree = tree["entities"].Children();
        }
        
        public IEnumerable<JToken> EntityTree;
        public Dictionary<Level, Rectangle> Neighbours { get; } = new Dictionary<Level, Rectangle>();

        public string Id { get; }
        public Point Size { get; }
        public Point Location { get; }

        public bool Loaded
        {
            get {
                return loaded;
            }
            set {
                if (value) {
                    Load();
                } else {
                    Unload();
                }
            }
        }

        private bool loaded = false;
        private Rectangle Bounds
        {
            get {
                return new Rectangle(Location, Size);
            }
        }

        public override string ToString()
        {
            return this.Id;
        }

        public void Load()
        {
            if (!loaded) {
                foreach (JObject e in EntityTree) {
                    Entity entity;
                    if (e["type"].ToString() == "Entity")
                        entity = e["object"].ToObject<Entity>();
                    else
                        entity = EntityFactory.Build(e["type"].ToString(), e["object"] as JObject);

                    // If object found that exists through another level already
                    if (e["object"]["id"] != null && GameState.Active.Entities.Any(i => i.Id == e["object"]["id"].ToString()))
                        continue;

                    if (e["sprites"] != null) {
                        if (e["sprites"]["animations"] != null) {
                            JObject anims = e["sprites"]["animations"] as JObject;
                            foreach (JProperty obj in anims.Properties()) {
                                entity.LoadAnimation(obj.Value["assetName"].ToString(), obj.Name, obj.Value["looping"].ToObject<bool>(), float.Parse(obj.Value["frameTime"]?.ToString() ?? "0.1"));
                                if (obj.Value["default"]?.ToString() == "true")
                                    entity.Show(obj.Name);
                            }
                        }

                        if (e["sprites"]["spritesheets"] != null) {
                            JObject sprites = e["sprites"]["spritesheets"] as JObject;
                            foreach (JProperty obj in sprites.Properties()) {
                                entity.LoadSprite(obj.Value["assetName"].ToString(), obj.Name, int.Parse(obj.Value["sheetIndex"]?.ToString() ?? "0"));
                                if (obj.Value["default"]?.ToString() == "true")
                                    entity.Show(obj.Name);
                            }
                        }
                    }

                    GameState.Active.Entities.Add(entity);
                }
                loaded = true;
            }
        }

        public void Unload()
        {
            for (int i = GameState.Active.Entities.Count - 1; i >= 0; --i) {
                Entity e = GameState.Active.Entities[i];
                if (e.LinkedLevels.Contains(this) && e.LinkedLevels.All(level => !level.loaded)) {
                    // TODO: save
                    GameState.Active.Entities.Remove(e);
                }
            }
            loaded = false;
        }
    }
}
