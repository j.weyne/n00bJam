﻿
using System.Linq;

namespace n00bJam
{
    public class GameState: State
    {
        public Level Current
        {
            get; set;
        }

        private Character player = null;

        public Character Player
        {
            get {
                player = player ?? this.Entities.Single(entity => entity.Id == "player") as Character;
                return player;
            }
        }

        public new static GameState Active
        {
            get {
                if (State.Active is GameState)
                    return (GameState)State.Active;
                else
                    return null;
            }
        }
    }
}
