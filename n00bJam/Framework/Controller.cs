﻿using Microsoft.Xna.Framework;

namespace n00bJam
{
    public abstract class Controller
    {
        /// TODO: Update
        public abstract void HandleInput(GameTime gameTime);

        public abstract void Update(GameTime gameTime);
    }
}
