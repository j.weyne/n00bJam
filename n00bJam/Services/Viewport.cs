﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace n00bJam
{
    public static class Viewport
    {
        public static Vector2 Camera { get; set; }
        public static Vector2 Zoom { get; set; }

        public static bool Fullscreen
        {
            get
            {
                return Game.graphics.IsFullScreen;
            }
            set
            {
                Game.graphics.IsFullScreen = value;
                Game.graphics.ApplyChanges();
            }
        }
        public static Point Screen
        {
            get
            {
                return Game.graphics.GraphicsDevice.Viewport.Bounds.Size;
            }
        }
    }
}