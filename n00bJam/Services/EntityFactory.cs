﻿using System;
using Newtonsoft.Json.Linq;

namespace n00bJam
{
    public static class EntityFactory
    {
        public static Entity Build(string type, JObject obj)
        {
            switch (type) {
                case "Character":
                    return obj.ToObject<Character>();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
