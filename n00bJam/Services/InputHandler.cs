﻿using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace n00bJam
{
    public enum Mouse
    {
        Left,
        Middle,
        Right
    }

    public enum Button
    {
        Pressed,
        Released,
        Down,
        Up
    }

    public static class InputHandler
    {
        private static KeyboardState prevKeyState = new KeyboardState();
        private static MouseState prevMouseState = new MouseState();
        private static KeyboardState KeyState = new KeyboardState();
        private static MouseState MouseState = new MouseState();

        public static bool Is(Keys key, Button s)
        {
            switch (s) {
                case Button.Down:
                    return KeyState.IsKeyDown(key);
                case Button.Up:
                    return KeyState.IsKeyUp(key);
                case Button.Pressed:
                    return KeyState.IsKeyDown(key) && prevKeyState.IsKeyUp(key);
                case Button.Released:
                    return KeyState.IsKeyUp(key) && prevKeyState.IsKeyDown(key);
                default:
                    return false;
            }
        }

        public static bool Is(Mouse button, Button s)
        {
            ButtonState prev;
            ButtonState now;
            switch (button) {
                case Mouse.Left:
                    now = MouseState.LeftButton;
                    prev = prevMouseState.LeftButton;
                    break;
                case Mouse.Middle:
                    now = MouseState.MiddleButton;
                    prev = prevMouseState.MiddleButton;
                    break;
                case Mouse.Right:
                    now = MouseState.RightButton;
                    prev = prevMouseState.RightButton;
                    break;
                default:
                    return false;
            }
            switch (s) {
                case Button.Down:
                    return now == ButtonState.Pressed;
                case Button.Up:
                    return now == ButtonState.Released;
                case Button.Pressed:
                    return now == ButtonState.Pressed && prev == ButtonState.Released;
                case Button.Released:
                    return now == ButtonState.Released && prev == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        public static void Update()
        {
            prevKeyState = KeyState;
            prevMouseState = MouseState;
            KeyState = Keyboard.GetState();
            MouseState = Microsoft.Xna.Framework.Input.Mouse.GetState();
        }
    }
}
