﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Newtonsoft.Json.Linq;

namespace n00bJam
{
    public class Game: Microsoft.Xna.Framework.Game
    {
        public static GraphicsDeviceManager graphics;
        public static SpriteBatch spriteBatch;
        public static new ContentManager Content;
        
        [STAThread]
        static void Main()
        {
            using (var game = new Game())
                game.Run();
        }

        public Game()
        {
            Game.Content = base.Content;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        protected override void LoadContent()
        {
            DisplayMode currentDisplay = graphics.GraphicsDevice.Adapter.CurrentDisplayMode;
            graphics.PreferredBackBufferWidth = currentDisplay.Width;
            graphics.PreferredBackBufferHeight = currentDisplay.Height;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();

            spriteBatch = new SpriteBatch(GraphicsDevice);
            new GameState();
            Level.Buffer();
            Level.LoadLevels(Point.Zero);
        }
        
        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            InputHandler.Update();
            State.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            State.Draw(gameTime, spriteBatch);

            base.Draw(gameTime);
        }
    }
}
